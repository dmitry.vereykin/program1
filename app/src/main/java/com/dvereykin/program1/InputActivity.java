package com.dvereykin.program1;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class InputActivity extends AppCompatActivity implements View.OnClickListener{

    private Button okButton;
    private Button clrButton;

    private EditText editName;
    private EditText editAddress;
    private EditText editCity;
    private EditText editState;
    private EditText editZipCode;

    private String data = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input);

        okButton = (Button) findViewById(R.id.cmdOK);
        clrButton = (Button) findViewById(R.id.cmdClear);

        editName = (EditText) findViewById(R.id.editNameFld);
        editAddress = (EditText) findViewById(R.id.editAddressFld);
        editCity = (EditText) findViewById(R.id.editCityFld);
        editState = (EditText) findViewById(R.id.editStateFld);
        editZipCode = (EditText) findViewById(R.id.editZipCodeFld);

        okButton.setOnClickListener(this);
        clrButton.setOnClickListener(this);

    }

    public void onClick(View v) {
        if (v == okButton) {

            if (editName.getText().toString().equals("")
                    || editAddress.getText().toString().equals("")
                    || editCity.getText().toString().equals("")
                    || editState.getText().toString().equals("")
                    || editZipCode.getText().toString().equals("")) {
                createDialog("Alert!", "One or more of the fields are empty");
            } else {

                data += editName.getText() + ", " + editAddress.getText() + ", "
                        + editCity.getText() + ", " + editState.getText() + ", "
                        + editZipCode.getText();
                createDialog("Success", data);
            }
        }

        if (v == clrButton) {
            clear();
        }
    }

    public void createDialog(String title, String data) {
        AlertDialog alertDialog = new AlertDialog.Builder(InputActivity.this).create();

        alertDialog.setTitle(title);
        alertDialog.setMessage(data);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int e) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public void clear() {
        editName.setText("");
        editAddress.setText("");
        editState.setText("");
        editCity.setText("");
        editZipCode.setText("");
        data = "";
    }
}
